var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Tutorial",
  "homePage" : "Main",
  "name" : "Tutorial",
  "platformType" : "WEB",
  "securityEnabled" : "false",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};